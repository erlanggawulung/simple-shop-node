const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');

exports.orders_get_all = (req, res, next) => {
    Order.find()
    .select('product quantity _id')
    .populate('product', 'name price')
    .exec()
    .then(docs => {
        console.log(docs);
        const response = {
            count: docs.length,
            orders: docs.map(doc => {
                return {
                    product: doc.product,
                    quantity: doc.quantity,
                    _id: doc._id,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/orders/'+doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

exports.orders_create_order = (req, res, next) => {
    Product.findById(req.body.productId)
    .then(product => {
        const order = new Order({
            _id: new mongoose.Types.ObjectId(),
            quantity: req.body.quantity,
            product: req.body.productId
        });
        if (product) {
            return order.save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    message: 'Order created successfully',
                    createdOrder: {
                        quantity: result.quantity,
                        _id: result._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/orders/'+result._id
                        }
                    }
                });    
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err
                });    
            });
        } else {
            res.status(404).json({message: 'No valid entry found provided Product ID.'});
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

exports.orders_get_order = (req, res, next) => {
    Order.findById(req.params.orderId)
    .populate('product', 'name price')
    .exec()
    .then(order => {
        if(order) {
            res.status(200).json({
                order: order,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders'
                }
            });
        } else {
            res.status(404).json({message: 'No valid entry found provided ID.'});
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});        
    });
};

exports.order_delete_order = (req, res, next) => {
    const id = req.params.orderId;
    Order.deleteOne({_id: id})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Order deleted',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/orders',
                body: {
                    productId: 'ID',
                    quantity: 'Number'
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
};
